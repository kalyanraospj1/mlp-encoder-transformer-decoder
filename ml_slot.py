from mlp_mixer import MlpMixer
from torch import nn
from brc_pytorch.layers import BistableRecurrentCell, NeuromodulatedBistableRecurrentCell
import torch
from models.transformer import TransformerDecoder,TransformerDecoderLayer
class SlotAttention(nn.Module):
    def __init__(self, num_slots, dim, iters=3, eps=1e-8, hidden_dim=128):
        super().__init__()
        self.num_slots = num_slots
        self.iters = iters
        self.eps = eps
        self.scale = dim ** -0.5

        self.slots_q = nn.Embedding(num_slots, dim)

        self.to_q = nn.Linear(dim, dim)
        self.to_k = nn.Linear(dim, dim)
        self.to_v = nn.Linear(dim, dim)

        self.gru = NeuromodulatedBistableRecurrentCell(dim, dim)

        hidden_dim = max(dim, hidden_dim)

        self.mlp = nn.Sequential(
            nn.Linear(dim, hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(hidden_dim, dim)
        )

        self.norm_input = nn.LayerNorm(dim)
        self.norm_slots = nn.LayerNorm(dim)
        self.norm_pre_ff = nn.LayerNorm(dim)

    def forward(self, inputs):
        b, n, d = inputs.shape
        n_s = self.num_slots

        slots = self.slots_q.weight.unsqueeze(0).repeat(b, 1, 1)

        inputs = self.norm_input(inputs)
        k, v = self.to_k(inputs), self.to_v(inputs)

        for _ in range(self.iters):
            slots_prev = slots

            slots = self.norm_slots(slots)
            q = self.to_q(slots)

            dots = torch.einsum('bid,bjd->bij', q, k) * self.scale
            attn = dots.softmax(dim=1) + self.eps
            attn = attn / attn.sum(dim=-1, keepdim=True)

            updates = torch.einsum('bjd,bij->bid', v, attn)

            slots = self.gru(
                updates.reshape(-1, d),
                slots_prev.reshape(-1, d)
            )

            slots = slots.reshape(b, -1, d)
            slots = slots + self.mlp(self.norm_pre_ff(slots))
        return slots

class PositionalEncoding(nn.Module):

    def __init__(self, d_model, dropout=0.0, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class DETRdemo(nn.Module):
    """
    Demo DETR implementation.

    Demo implementation of DETR in minimal number of lines, with the
    following differences wrt DETR in the paper:
    * learned positional encoding (instead of sine)
    * positional encoding is passed at input (instead of attention)
    * fc bbox predictor (instead of MLP)
    The model achieves ~40 AP on COCO val5k and runs at ~28 FPS on Tesla V100.
    Only batch size 1 supported.
    """

    def __init__(self, num_classes=91, hidden_dim=512):
        super().__init__()

        # create ResNet-50 backbone
        # model=timm.create_model('efficientnet_b4',pretrained=True, scriptable=True)
        model = MlpMixer(img_size=800, patch_size=50)

        self.backbone = nn.Sequential(*list(model.children())[:-1])
        '''
        for parameter in self.backbone.parameters():
            parameter.requires_grad = False
        # create conversion layer
        self.conv_1=nn.Conv2d(1792,256,1)
        self.max_1=nn.MaxPool2d(3,1)
        self.conv_2=nn.Conv2d(256,128,3)
        self.max_2=nn.MaxPool2d(3,1)
        self.pos=nn.Embedding(128, hidden_dim)
        self.attn = AoA(dim = hidden_dim,heads = 8)
        '''


        self.attention = SlotAttention(num_slots=300, dim=hidden_dim, iters=3)
        self.linear_class_3 = nn.Linear(hidden_dim, num_classes + 1)
        self.linear_bbox = nn.Linear(hidden_dim, 4)

    def forward(self, inputs):
        # propagate inputs through ResNet-50 up to avg-pool layer
        x = self.backbone(inputs)
        h = self.attention(x)

        return {'pred_logits': self.linear_class_3(h),
                'pred_boxes': self.linear_bbox(h).sigmoid()}

class DETRdemo_1(nn.Module):
    """
    Demo DETR implementation.

    Demo implementation of DETR in minimal number of lines, with the
    following differences wrt DETR in the paper:
    * learned positional encoding (instead of sine)
    * positional encoding is passed at input (instead of attention)
    * fc bbox predictor (instead of MLP)
    The model achieves ~40 AP on COCO val5k and runs at ~28 FPS on Tesla V100.
    Only batch size 1 supported.
    """
    def __init__(self, num_classes=6, hidden_dim=256):
        super().__init__()

        # create ResNet-50 backbone
        #model=timm.create_model('efficientnet_b4',pretrained=True, scriptable=True)
        model = MlpMixer(img_size=800, patch_size=50)

        self.backbone=nn.Sequential(*list(model.children())[:-1])
        '''
        for parameter in self.backbone.parameters():
            parameter.requires_grad = False
        # create conversion layer
        self.conv_1=nn.Conv2d(1792,256,1)
        self.max_1=nn.MaxPool2d(3,1)
        self.conv_2=nn.Conv2d(256,128,3)
        self.max_2=nn.MaxPool2d(3,1)
        self.pos=nn.Embedding(128, hidden_dim)
        self.attn = AoA(dim = hidden_dim,heads = 8)
        '''
        self.norm=nn.LayerNorm(256)
        self.decoder_layer = TransformerDecoderLayer(d_model=256, nhead=8)
        self.transformer_decoder = TransformerDecoder(self.decoder_layer, num_layers=6,norm=self.norm,return_intermediate=True)
        self.query_pos = nn.Embedding(10,256)
        #self.attention=SlotAttention(num_slots = 100,dim = hidden_dim,iters = 3 )
        self.linear_class_3 = nn.Linear(hidden_dim, num_classes + 1)
        self.linear_bbox = nn.Linear(hidden_dim, 4)

    def forward(self, inputs,aux_loss=None):
        # propagate inputs through ResNet-50 up to avg-pool layer
        x = self.backbone(inputs)
        b,_,_=x.shape
        que=self.query_pos.weight.unsqueeze(0).repeat(b, 1, 1).permute(1, 0, 2).cuda()
        mem=x.permute(2, 0, 1).cuda()
        x=self.transformer_decoder(que,mem).transpose(1, 2)
        #h=self.attention(x)
        outputs_class = self.linear_class_3(x)
        outputs_coord = self.linear_bbox(x).sigmoid()
        out = {'pred_logits': outputs_class[-1], 'pred_boxes': outputs_coord[-1]}
        if aux_loss:
            out['aux_outputs'] = self._set_aux_loss(outputs_class, outputs_coord)
        return out

    @torch.jit.unused
    def _set_aux_loss(self, outputs_class, outputs_coord):
        # this is a workaround to make torchscript happy, as torchscript
        # doesn't support dictionary with non-homogeneous values, such
        # as a dict having both a Tensor and a list.
        return [{'pred_logits': a, 'pred_boxes': b}
                for a, b in zip(outputs_class[:-1], outputs_coord[:-1])]


model = DETRdemo()